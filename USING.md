# How to use BuildStream inside a container

BuildStream docker images are [available on Docker
Hub](https://hub.docker.com/r/buildstream/buildstream).

Note that at time of writing, Buildstream 2 is currently in development,
so `dev` and `nightly` images will be of Buildstream 2. Buildstream 2
introduces important changes to the API.


## Toolbox
[Toolbox](https://github.com/containers/toolbox) spawns interactive containers,
using podman.

You can create and enter a BuildStream toolbox with the following commands:

    toolbox create -i docker.io/buildstream/buildstream:dev
    toolbox enter buildstream-dev

## Docker
We recommend using the
[`bst-here` wrapper script](https://github.com/apache/buildstream/blob/master/contrib/bst-here)
which automates the necessary container setup. You can download it and make
it executable like this:

    mkdir -p ~/.local/bin
    curl --get https://raw.githubusercontent.com/apache/buildstream/master/contrib/bst-here > ~/.local/bin/bst-here
    chmod +x ~/.local/bin/bst-here

Check if `~/.local/bin` appears in your `PATH` environment variable -- if it
doesn't, you should edit your `~/.bashrc` so that it does:

    export PATH="${PATH}:${HOME}/.local/bin"

Once the script is available in your `PATH`, you can run `bst-here` to open a
shell session inside a new container based off the latest version of the
`buildstream/buildstream` Docker image. The current working directory will be
mounted inside the container at `/src`.

You can also run individual BuildStream commands as `bst-here COMMAND`. For
example: `bst-here show systems/my-system.bst`. Note that BuildStream won't
be able to integrate with Bash tab-completion if you invoke it in this way.

By default, the `latest` tag of the `buildstream/buildstream` image will be
used. This tag tracks the latest stable release of BuildStream. You can choose
to use a different tag using the `-j` option.  For example, you can run the
nightly build of BuildStream like so:

    bst-here -j nightly

More details on image variants can be found [on Docker Hub](
https://hub.docker.com/r/buildstream/buildstream).

Two Docker volumes are set up by the `bst-here` script:

 - `buildstream-cache --` mounted at `~/.cache/buildstream`
 - `buildstream-config --` mounted at `~/.config/`

These are necessary so that your BuildStream cache and configuration files
persist between invocations of `bst-here`.
